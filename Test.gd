extends Spatial

export(int) var size = 15
export var interpolation = true

var SmallCube = preload("res://SmallCube.tscn") 

func _ready():
	# Create sizexzise small cubes with 1m of separation
	var counter: int = 0
	for i in size:
		for j in size:
			var new_cube = SmallCube.instance()
			new_cube.translation = Vector3(i, 0.5, 10 - j)
			new_cube.interpolation_active = interpolation
			new_cube.name = 'cube'+str(counter)
			self.add_child(new_cube)
			counter += 1