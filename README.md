# Godot Online Synchronization

## Motivation

Me and my team are creating an online multiplayer TPS, for this type of game, we use the [tribes networking model](https://www.gamedevs.org/uploads/tribes-networking-model.pdf), in other words, we use a client/server architecture with a dedicated and authoritative server.

I created this for my own learning and later implementation into our game, and since none of the official godot networking demos uses the tribes model neither show any latency compesation solution, it is good to share this with the communtiy and build it togheter.

## Background knowledge

If you want to learn games networking properly I recommend the book [Multiplayer Game Programming: Architecting Networked Games](https://www.amazon.com/Multiplayer-Game-Programming-Architecting-Networked/dp/0134034309) by Joshua Gazler and Sanjay madhav.

This demo itself takes inspiration from the GDC talk [Networking for physics programmers](https://www.youtube.com/watch?v=Z9X4lysFr64) by Glenn Fieddler and the [Fast-Paced Multiplayer article series](https://www.gabrielgambetta.com/client-server-game-architecture.html) by Gabriel Gambetta.

Other very useful resources:
 
Valve wiki: https://developer.valvesoftware.com/wiki/Latency_Compensating_Methods_in_Client/Server_In-game_Protocol_Design_and_Optimization

Networking of Halo Reach: https://www.gdcvault.com/play/1014345/I-Shot-You-First-Networking

## What you can find in this demo

The aim for this demo is to show a guide on state replication and latency compensation using Godot high level networking API. Featuring:

- [x] Basic interpolation
- [x] Interpolation with package drop
- [x] Client-server reconciliation

## How to use this demo

Run 2 instances of the game, choose one as server and the other as client. You will see the test scene running after that. Use arrow keys to move (on the client).

![Test scene](https://gitlab.com/Juankz/realtime-network-sync-godot/uploads/2f7474d2f206e4aec423a5a35d27ce01/picture_test_scene.png)

Use an external tool to simulate network latency (100ms is acceptable) and packet lost (2% is acceptable): 

**Linux**: Use traffic control (tc). It is the tool for the job: https://wiki.linuxfoundation.org/networking/netem#examples

**Windows**: Install clumsy, it should be enough: http://jagt.github.io/clumsy/index.html

You can enable/disable the interpolation algorithm under Test Scene > Spatial Node > interpolation checkbox 

![Interpolation switch](https://gitlab.com/Juankz/realtime-network-sync-godot/uploads/0bd2181891a6ad36e1754a72de7b57a1/interpolation_switch.png)


You can also enable/disable the client-server reconciliation algorithm under Test Scene > Player > csr checkbox

![Client-server reconciliation switch](https://gitlab.com/Juankz/realtime-network-sync-godot/uploads/a5884ba8ab322a32894012424f859c0f/reconciliation_switch.png)

## Caveats

This code is not optimized at all. For example we are sending data for each cube every frame even if it's idle, in _Networking for physics programmers_ you can find more information about optimizations.

Because of the previous point, if the number of cubes increases, information overload will occur and either the client or the server will crash. 

## Contributing

This is open source, contribute by opening issues, contributing code(fixing typos, code comments, code, etc.). You can ask questions on the networking channel of Godots discord.
